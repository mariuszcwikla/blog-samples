package com.gitlab.mariuszcwikla.algorithms;

import java.util.concurrent.ThreadLocalRandom;

import com.gitlab.mariuszcwikla.algorithms.OrderMaintenanceList.Node;

/**
 * Corner-case test that inserts record behind head and behind tail at the same time
 *
 */
class OrderMaintenanceListInsertAfterHeadAndTailTest {

	public static void main(String[] args) {
		
		OrderMaintenanceList<Integer> list = new OrderMaintenanceList<Integer>();
        Node<Integer> head = list.insertFirst(0);
        Node<Integer> tail = head;
        int n = 0;
        while(true) {
        	Node<Integer> newTail = list.insertAfter(tail, ThreadLocalRandom.current().nextInt());
        	if (!tail.after(newTail) && !newTail.after(tail)) {
        		System.out.println("Failure (tail) after " + n + " iterations");
        		break;
        	}
        	
        	tail = newTail;
            Node<Integer> next = list.insertAfter(head, ThreadLocalRandom.current().nextInt());
            if (!next.after(head) && !next.after(head)) {
            	System.out.println("Failure (head) after " + n + " iterations");
            	break;
            }
            n++;
            if (n % 10_000 == 0) {
                System.out.println(String.format("iteration %d, next to head = %s, tail = %s", n, next.getLabel().toString(), tail.getLabel().toString()));
                
                Node<Integer> node = head;
                while (node.getNext() != null) {
                	if (!node.getNext().after(node)) {
                		System.out.println("broken ordering of nodes " + node.getLabel().toString() + " and " + node.getNext().getLabel().toString());
                		break;
                	}
                	node = node.getNext();
                }
            }
        }
	}

}
