package com.gitlab.mariuszcwikla.algorithms;

public class Util {

    public static int bitsOf(int value) {
        for (int i=31; i>=0; i--) {
            if((value & (1<<i)) != 0)
                return i+1;
        }
        return 1;
    }
}
