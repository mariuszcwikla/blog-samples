package com.gitlab.mariuszcwikla.algorithms;

import java.util.concurrent.ThreadLocalRandom;

import com.gitlab.mariuszcwikla.algorithms.OrderMaintenanceList.Node;

/**
 * Corner-case test that inserts record behind head and behind tail at the same time
 *
 */
class OrderMaintenanceListInsertFirstTest {

	public static void main(String[] args) {
		
		OrderMaintenanceList<Integer> list = new OrderMaintenanceList<Integer>();
		list.insertFirst(5);
        int n = 0;
        while(true) {
            Node<Integer> head = list.insertFirst(ThreadLocalRandom.current().nextInt());
            if (!head.after(head.getNext()) && !head.getNext().after(head)) {
            	System.out.println("Failure (head) after " + n + " iterations");
            	break;
            }
            n++;
            if (n % 10_000 == 0) {
                System.out.println(String.format("iteration %d, next to head = %s", n, head.getLabel().toString()));
                
                Node<Integer> node = head;
                while (node.getNext() != null) {
                	if (!node.getNext().after(node)) {
                		System.out.println("broken ordering of nodes " + node.getLabel().toString() + " and " + node.getNext().getLabel().toString());
                		break;
                	}
                	node = node.getNext();
                }
            }
        }
	}

}
