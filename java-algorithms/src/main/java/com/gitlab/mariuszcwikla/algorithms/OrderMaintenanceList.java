package com.gitlab.mariuszcwikla.algorithms;

import java.util.concurrent.ThreadLocalRandom;
import org.apache.commons.math3.fraction.Fraction;

public class OrderMaintenanceList<T> {
    public static class Node<T>{
        private T value;
        private Fraction label;
        private Node<T> next;

        public Node(Node<T> next, T value, Fraction label) {
            this.next = next;
            this.value = value;
            this.label = label;
        }
        
        public boolean after(Node<T> other) {
            return label.compareTo(other.label) > 0;
        }

        public T getValue() {
            return value;
        }

        public Fraction getLabel() {
            return label;
        }

        public Node<T> getNext() {
			return next;
		}

		public int size() {
            int size = 0;
            Node<T> n = this;
            while(n!=null) {
                n = n.next;
                size += 1;
            }
            return size;
        }
    }
    
    private Node<T> head = null;
    
    public Node<T> insertFirst(T value){
        Fraction fraction;
        if (head != null) {
            fraction = new Fraction(head.label.getNumerator() - 1, head.label.getDenominator());
        } else {
            fraction = new Fraction(0, 1);
        }
            
        head = new Node<T>(head, value, fraction);
        return head;
    }
    
    public Node<T> insertAfter(Node<T> node, T value){
        Fraction fraction;
        if (node.next == null) {
            fraction = new Fraction(node.label.getNumerator() + 1, node.label.getDenominator());
        } else {
            fraction = new Fraction(node.label.getNumerator() + node.next.label.getNumerator(),
                    node.label.getDenominator() + node.next.label.getDenominator());
        }
        
        node.next = new Node<T>(node.next, value, fraction);
        return node.next;
    }
    
    public static void main(String[] args) {
        OrderMaintenanceList<Integer> list = new OrderMaintenanceList<Integer>();
        Node<Integer> head = list.insertFirst(0);
        int n = 0;
        while(true) {
            Node<Integer> next = list.insertAfter(head, ThreadLocalRandom.current().nextInt());
            if (!next.after(head) && !next.after(head)) {
                System.out.println("Failure after " + n + " iterations");
                break;
            }
            n++;
            if (n % 100_000 == 0) {
                int bits = Util.bitsOf(next.label.getDenominator());
                System.out.println(String.format("iteration %d, fraction = %s bits of denominator=%d", n, next.label.toString(), bits));
            }
        }
    }

}
