package com.gitlab.mariuszcwikla.algorithms;

import java.util.concurrent.ThreadLocalRandom;

public class OrderMaintenanceListDouble<T> {
    public static class Node<T>{
        private T value;
        private double label;
        private Node<T> next;

        public Node(Node<T> next, T value, double label) {
            this.next = next;
            this.value = value;
            this.label = label;
        }
        
        public boolean after(Node<T> other) {
            return label > other.label;
        }

        public T getValue() {
            return value;
        }

        public double getLabel() {
            return label;
        }
    }
    
    private Node<T> head = null;
    
    public Node<T> insertFirst(T value){
        double label;
        if (head != null) {
            label = head.label - 1.0f;
        } else {
            label = 0.0f;
        }
            
        head = new Node<T>(head, value, label);
        return head;
    }
    
    public Node<T> insertAfter(Node<T> node, T value){
        double label;
        if (node.next == null) {
            label = node.label + 1.0f;
        } else {
            label = (node.label + node.next.label) / 2.0f;
        }
        
        node.next = new Node<T>(node.next, value, label);
        return node.next;
    }
    
    public static void main(String[] args) {
        OrderMaintenanceListDouble<Integer> list = new OrderMaintenanceListDouble<Integer>();
        Node<Integer> head = list.insertFirst(0);
        int n = 0;
        while(true) {
            Node<Integer> next = list.insertAfter(head, ThreadLocalRandom.current().nextInt());

            if (!next.after(head) && !next.after(head)) {
                System.out.println("Failure after " + n + " iterations");
                break;
            }
            n++;
            if (n%10_000==0) {
                System.out.println(String.format("iteration=%d value=%f", n, next.getLabel()));
            }
        }
    }
}
