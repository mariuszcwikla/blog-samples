package com.example.demo.product;

import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.demo.HealthcheckTestMixin;

@SpringBootTest
@AutoConfigureMockMvc
class ProductServiceTests implements HealthcheckTestMixin {

}
